# Postgresql logical backup with restic

This container performs a backup of a postgresql instance by:

- Performing a `pg_dumpall` of all databases with restic
- Sending the output to remote storage (currently only S3-compatible supported)

## Environment variables

| Variable name         | Description                                                                                                                                      |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| S3_ENDPOINT           | Location of your S3 storage. Defaults to "https://s3.wasabisys.com"                                                                            |
| AWS_ACCESS_KEY_ID     | Key id for s3 storage. Required                                                                                                                  |
| AWS_SECRET_ACCESS_KEY | Key secret for s3 storage. Required                                                                                                              |
| POSTGRESQL_HOST          | Your database host. Required                                                                                                                     |
| POSTGRESQL_USER          | Defaults to "postgres"                                                                                                                               |
| POSTGRESQL_PORT          | Defaults to 5432                                                                                                                                 |
| POSTGRESQL_PWD           | Password to connect to database. One of POSTGRESQL_PWD or POSTGRESQL_PWD_FILE must be provided                                                         |
| POSTGRESQL_PWD_FILE      | File containing the password to connect to database. One of POSTGRESQL_PWD or POSTGRESQL_PWD_FILE must be provided                                     |
| BUCKET_NAME           | S3 bucket for your Restic repository. The bucket must exist, but if a Restic repository doesn't, this container will create it for you. Required |
| FILENAME              | Filename/path restic will use for your snapshots. Defaults to 'postgresql'                                                                          |
| ENABLE_PRUNE          | Whether to forget and prune old snapshots. Value is either 0 or 1. Default is `1`                                                                |
| KEEP_LAST             | How many snapshots to keep. Defaults to 100                                                                                                      |
| RESTIC_PASSWORD       | Restic password env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                         |
| RESTIC_PASSWORD_FILE  | Restic password file env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                    |

## Example

1. Run postgres

```bash
docker network create restic
docker run -d --name postgresql --net restic -e POSTGRESQL_PASSWORD=123 bitnami/postgresql:13.3.0-debian-10-r43
```

2. Run the postgresql dump container

```
docker run --rm \
  -e BUCKET_NAME=<bucket> \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  -e RESTIC_PASSWORD=123 \
  -e POSTGRESQL_PWD=123 \
  -e POSTGRESQL_HOST=postgresql \
  --net restic \
  j3asolutions/restic-postgresql
```

3. Check everything is working fine using restic cli in your local machine


```
RESTIC_REPOSITORY="s3:https://s3.wasabisys.com/<bucket>" \
AWS_ACCESS_KEY_ID=<key> \
AWS_SECRET_ACCESS_KEY=<secret> \
restic -r s3:https://s3.wasabisys.com/<bucket> restore latest --target dump-restore
```