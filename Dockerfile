#
# LINKERD
#

# FROM docker.io/curlimages/curl:latest as linkerd
# ARG LINKERD_AWAIT_VERSION=v0.2.6
# RUN curl -sSLo /tmp/linkerd-await https://github.com/linkerd/linkerd-await/releases/download/release%2F${LINKERD_AWAIT_VERSION}/linkerd-await-${LINKERD_AWAIT_VERSION}-amd64 && \
#     chmod 755 /tmp/linkerd-await

#
# RUNTIME
#
FROM docker.io/bitnami/postgresql:15.2.0-debian-11-r10

USER root

ENV S3_ENDPOINT "https://s3.fr-par.scw.cloud"
ENV POSTGRESQL_USER "postgres"
ENV POSTGRESQL_PORT "5432"
ENV FILENAME "postgresql"
ENV RESTIC_DIR "/etc/restic"
ENV ENABLE_PRUNE 1
ENV KEEP_LAST 100

RUN install_packages restic curl \
    && mkdir $RESTIC_DIR \
    && restic self-update

# COPY --from=linkerd /tmp/linkerd-await /linkerd-await
COPY backup.sh $RESTIC_DIR

RUN chown -R 1001:1001 $RESTIC_DIR \
    && chmod u+x $RESTIC_DIR/backup.sh

USER 1001

# ENTRYPOINT ["/linkerd-await", "--shutdown", "--"]
# CMD ["/bin/bash", "/etc/restic/backup.sh"]
CMD ["/etc/restic/backup.sh"]