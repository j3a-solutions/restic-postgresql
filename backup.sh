#!/bin/bash

set -euo pipefail

export RESTIC_REPOSITORY=s3:$S3_ENDPOINT/$BUCKET_NAME
export PGPASSWORD=

if [ -z "${POSTGRESQL_PWD_FILE+x}" ]
then
  PGPASSWORD=$POSTGRESQL_PWD
else
  PGPASSWORD=$(cat $POSTGRESQL_PWD_FILE)
fi

echo "Starting backup with $(restic version)"

echo "Checking snapshots and whether repository exists";
restic snapshots --cache-dir $RESTIC_DIR && REPO_EXISTS=$? || REPO_EXISTS=$?

if [ ! $REPO_EXISTS -eq 0 ]
then
  echo "Creating repository in $RESTIC_REPOSITORY"
  restic init;
fi

pg_isready \
 --host=$POSTGRESQL_HOST \
 --port=$POSTGRESQL_PORT \
 --username=$POSTGRESQL_USER \
 &>/dev/null && CONNECTION_OK=$? || CONNECTION_OK=$?

if [ ! $CONNECTION_OK -eq 0 ]
then
  echo "Unable to connect to database"
  exit 1;
fi

BACKUP_ARGS="--host=$POSTGRESQL_HOST --port=$POSTGRESQL_PORT --username=$POSTGRESQL_USER --compress=${COMPRESS_LEVEL:-9}"
COMMAND=

if [ -z ${POSTGRESQL_DB+x} ]; then 
  COMMAND="pg_dumpall $BACKUP_ARGS"
else
  COMMAND="pg_dump $BACKUP_ARGS $POSTGRESQL_DB"
fi

eval $COMMAND | restic backup --cache-dir $RESTIC_DIR --stdin --verbose --stdin-filename $FILENAME.gz;

echo "Postgress dump added to restic repository";

if [ $ENABLE_PRUNE -eq 1 ]
then
  restic forget --prune --group-by paths --cache-dir $RESTIC_DIR --keep-last $KEEP_LAST;
fi